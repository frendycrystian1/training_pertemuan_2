/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dannys.training_pert_2;

/**
 *
 * @author Dankichi Kuroro
 */
public class Job {

    protected int hp;
    protected int mp;
    protected String weapon;
    protected int damage;
    protected int critRate;

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getHp() {
        return hp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public int getMp() {
        return mp;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getDamage() {
        return damage;
    }

    public void setCritRate(int critRate) {
        this.critRate = critRate;
    }

    public int getCritRate() {
        return critRate;
    }
    
    public Job(int hp, int mp, String weapon, int damage, int critRate) {
		super();
                this.hp=hp;
                this.mp=mp;
                this.weapon=weapon;
                this.damage=damage;
                this.critRate=critRate;       
    }
    
}
